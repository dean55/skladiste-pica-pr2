#ifndef DATATYPE
#define DATATYPE

typedef struct korisnik {
	int id;
	char ime[20];
	char prezime[20];
	char opis_posla[20];
	char korisnicko_ime[20];
	char hash_lozinka[35];
	char status[20];
}KORISNIK;

typedef struct sva_pica {
	int id;
	char* naziv;
	char gazirano;
	char tip;
	int kolicina;
	float* zapremina;
	float* cijena;
	struct sva_pica* sljedeci_cvor;
}PICE;

#endif