#define _CRT_SECURE_NO_WARNINGS
#include "deklaracije_funkcija.h"
#include "deklaracije_funkcija_artikala.h"
#include "strukture.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <windows.system.h>

static int status = 0;
extern int trazeni_id_2;
extern KORISNIK admin = { -1,"admin","admin","admin","administrator","hfignlecrp20qomkljmkge9797asdfcv","ADMIN" };
FILE* datoteka;
FILE* datoteka2;
PICE* glavni_cvor;
int broj_artikala;
int id;

int pocetni_izbornik(void) {
	char izbor;
	system("cls");
	printf("#######################################################################################################################\n");
	printf("#######################################################################################################################\n");
	printf("##                                                                                                                   ##\n");
	printf("##  SKLADISTE PICA                                                                                                   ##\n");
	printf("##                                                                                                                   ##\n");
	printf("##  1.Prijava                                                                                                        ##\n");
	printf("##  2.Registracija                                                                                                   ##\n");
	printf("##  3.O programu                                                                                                     ##\n");
	printf("##  4.Izlaz iz programa                                                                                              ##\n");
	printf("##                                                                                                                   ##\n");
	printf("#######################################################################################################################\n");
	printf("#######################################################################################################################\n");
	izbor = _getch();
	switch (izbor) {
	case '1':
		do {
			status = login_forma();
			if (status == 2) {
				while (admin_mod_izbornik(0));
				status = 0;
			}
			if (status == 3) {
				while (admin_mod_izbornik(1));
				status = 0;
			}
			if (status == 4) {
				printf("\nVas racun jos nije odobren");
				_getch();
			}
			if (status == 5) {
				fclose(datoteka);
				kreiranje_datoteke_2();
				datoteka2 = fopen("Bezalkoholna pica.bin", "rb+");
				if (datoteka2 == NULL) {
					return 0;
				}
				broj_artikala = vrati_broj_artikala();
				id = vrati_id_2();
				if (broj_artikala != 0) {
					glavni_cvor = ucitavanje_pica(glavni_cvor, broj_artikala);
				}
				fclose(datoteka2);
				while (izbornik_artikli(1));
				datoteka = fopen("korisnicki racuni.bin", "rb+");
				if (datoteka == NULL) {
					perror("Otvaranje datoteke");
					exit(EXIT_FAILURE);
				}
				return 1;
				break;
			}
		}while (status);
		return 1;
		break;
	case '2':
		do {
			system("cls");
			printf("#######################################################################################################################\n");
			printf("##                                                REGISTRACIJA                                                       ##\n");
			printf("#######################################################################################################################\n");
			printf("\n");
		} while (forma());
		return 1;
		break;
	case '3':
		ispis_tekstualne_datoteke();
		return 1;
		break;
	case '4':
		if (izlaz() == 1) {
			return 0;
		}
		return 1;
		break;
	default:
		return 1;
	}
}
