#define _CRT_SECURE_NO_WARNINGS
#include "deklaracije_funkcija.h"
#include "deklaracije_funkcija_artikala.h"
#include "strukture.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <windows.system.h>

int trazeni_id_2;
static int broj_racuna;
static KORISNIK* racuni = NULL;
static char* korisnicko_ime = NULL;
FILE* datoteka;

extern FILE* datoteka2 = NULL;
extern PICE* glavni_cvor = NULL;
extern int broj_artikala;
extern int id;


int admin_mod_izbornik(int index) {
	char izbor;
	if (racuni != NULL) {
		free(racuni);
		racuni = NULL;
	}
	broj_racuna = broj_korisnika();
	racuni = ucitaj_korisnike(racuni, broj_racuna);
	if (trazeni_id_2 == -1) {
		korisnicko_ime = "administrator";
	}
	else {
		korisnicko_ime = vrati_korisnicko_ime(racuni, trazeni_id_2, broj_racuna);
	}
	
	system("cls");
	printf("#######################################################################################################################\n");
	printf("#######################################################################################################################\n\n");
	printf(" Ovlasti:%s\n",index==0 ? "ADMIN":"MODERATOR");
	printf(" Vase korisnicko ime: %s", korisnicko_ime);
	if (index == 0) {
		printf(" \tBroj zahtjeva za dodjelu prava: %d\n\n", vrati_broj_zahtjeva(racuni, broj_racuna));
		printf("  1.Korisnicki racuni ");
	}
	if (index == 1) {
		printf("\n");
	}
	printf("\n  2.Administracija pica\n");
	if (index == 0) {
		printf("  3.Brisanje svih korisnika\n");
	}
	printf("  4.Brisanje svih pica\n");
	printf("  5.Odjava\n\n");
	printf("#######################################################################################################################\n");
	printf("#######################################################################################################################\n");
	izbor = _getch();
	switch (izbor) {
	case '1':
		if (index == 1) {
			return 1;
		}
		while (korisnici());
		return 1;
		break;
	case '2':
		fclose(datoteka);
			kreiranje_datoteke_2();
			datoteka2 = fopen("Bezalkoholna pica.bin", "rb+");
			if (datoteka2 == NULL) {
				return 0;
			}
			broj_artikala = vrati_broj_artikala();
			id = vrati_id_2();
			if (broj_artikala != 0) {
				glavni_cvor = ucitavanje_pica(glavni_cvor, broj_artikala);
			}
			fclose(datoteka2);
			while (izbornik_artikli(0));
			datoteka = fopen("korisnicki racuni.bin", "rb+");
			if (datoteka == NULL) {
				perror("Otvaranje datoteke");
				exit(EXIT_FAILURE);
			}
		return 1;
		break;
	case '3':
		if (index == 1) {
			return 1;
		}
		system("cls");
		printf("jeste li sigurni da zelite obrisati sve korisnike\npritisnite [d] za nastavak");
		if (_getch() == 'd') {
			fclose(datoteka);
			brisanje_datoteke_korisnika(0);
			datoteka = fopen("korisnicki racuni.bin", "rb+");
			if (datoteka == NULL) {
				perror("Otvaranje datoteke");
				exit(EXIT_FAILURE);
			}
		}
		return 1;
		break;
	case '4':
		system("cls");
		printf("jeste li sigurni da zelite obrisati sva pica\npritisnite [d] za nastavak");
		if (_getch() == 'd') {
			fclose(datoteka);
			brisanje_datoteke_korisnika(1);
			datoteka = fopen("korisnicki racuni.bin", "rb+");
			if (datoteka == NULL) {
				perror("Otvaranje datoteke");
				exit(EXIT_FAILURE);
			}
		}
		return 1;
		break;
	case '5':
		if (izlaz() == 1) {
			if (racuni != NULL) {
				free(racuni);
				racuni = NULL;
			}
			korisnicko_ime = NULL;
			broj_racuna = 0;
			trazeni_id_2 = 0;
			return 0;
		}
		return 1;
		break;
	default:
		return 1;
	}
}