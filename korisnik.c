#define _CRT_SECURE_NO_WARNINGS
#include "deklaracije_funkcija.h"
#include "strukture.h"
#include <stdio.h>
#include <stdlib.h>
#include <windows.system.h>

FILE* datoteka;
static int broj_racuna = 0, trazeni_id = 0;
static KORISNIK* racuni = NULL;
static int index_ispisa = 0;

int korisnici(void) {
	char izbor;
	system("cls");
	printf("#######################################################################################################################\n");
	printf("## r.br   id    ime                     prezime                 opis posla              korisnicko ime  status       ##\n");
	printf("#######################################################################################################################\n");
	broj_racuna = 0;
	broj_racuna = broj_korisnika();
	if (broj_racuna == 0) {
		printf("Ne postoji niti jedan korisnik osim systemskog administratora\n");
	}
	if (broj_racuna != 0) {
		racuni = ucitaj_korisnike(racuni, broj_racuna);
		ispisivanje_korisnika(racuni, broj_racuna, index_ispisa);
	}
	printf("\nUkupan broj korisnika:%d\n", broj_racuna);
	printf("#######################################################################################################################\n");
	printf("##  [1] Dodaj korisnika   [2] Obrisi korisnika   [3] Azuriranje korisnika                                            ##\n");
	printf("##  Sortiranje imena: [4] a-z  [5] z-a  prezimena: [6] a-z  [7] z-a   [x] izlaz                                      ##\n");
	printf("##  Ispis  [s] svih korisnika [n] korisnika kojima status nije definiran                                             ##\n");
	printf("#######################################################################################################################\n");
	izbor = _getch();
	switch (izbor) {
	case '1':
		do {
			system("cls");
			printf("#######################################################################################################################\n");
			printf("##                                                  NOVI KORISNIK                                                    ##\n");
			printf("#######################################################################################################################\n");
			printf("\n");
		} while (forma());
		free(racuni);
		racuni = NULL;
		return 1;
		break;
	case '2':
		trazeni_id = 0;
		trazeni_id = pretrazivanje_korisnika_putem_ida(racuni, broj_racuna);
		if (trazeni_id == -1) {
			return 1;
		}
		else if (trazeni_id == -2) {
			return 1;
		}
		int id;
		rewind(datoteka);
		fseek(datoteka, sizeof(int), SEEK_CUR);
		fread(&id, sizeof(int), 1, datoteka);
		brisanje_korisnika(racuni, broj_racuna, trazeni_id, id);
		free(racuni);
		racuni = NULL;
		return 1;
		break;
	case'3':
		trazeni_id = 0;
		trazeni_id = pretrazivanje_korisnika_putem_ida(racuni, broj_racuna);
		if (trazeni_id == -1) {
			return 0;
		}
		else if (trazeni_id == -2) {
			return 1;
		}
		do {
			system("cls");
			printf("#######################################################################################################################\n");
			printf("##                                               AZURIRANJE KORISNIKA                                                ##\n");
			printf("#######################################################################################################################\n");
			printf("\n");
		} while (azuriranje(trazeni_id));
		free(racuni);
		racuni = NULL;
		return 1;
		break;
	case '4':
		sortiranje(racuni, broj_racuna, 1);
		free(racuni);
		racuni = NULL;
		return 1;
		break;
	case '5':
		sortiranje(racuni, broj_racuna, 2);
		free(racuni);
		racuni = NULL;
		return 1;
		break;
	case '6':
		sortiranje(racuni, broj_racuna, 3);
		free(racuni);
		racuni = NULL;
		return 1;
		break;
	case '7':
		sortiranje(racuni, broj_racuna, 4);
		free(racuni);
		racuni = NULL;
		return 1;
		break;
	case 's':
		index_ispisa = 0;
		return 1;
		break;
	case 'n':
		index_ispisa = 1;
		return 1;
		break;
	case 'x':
		free(racuni);
		racuni = NULL;
		return 0;
		break;
	default:
		return 1;
		break;
	}
}