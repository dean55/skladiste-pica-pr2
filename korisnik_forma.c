#define _CRT_SECURE_NO_WARNINGS
#include "deklaracije_funkcija.h"
#include "strukture.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <windows.system.h>

static char* ime = NULL, * prezime = NULL, * opis_posla = NULL, * korisnicko_ime = NULL, * lozinka = NULL, * lozinka_2 = NULL;
static char* hash_lozinka = NULL, * hash_lozinka_2 = NULL;
static int status_ime = 0, status_prezime = 0, status_opis_posla = 0, status_lozinka = 0, status_lozinka_2 = 0, status_korisnicko_ime = 0;
static int zastavica = 0;

int forma(void) {
	char izbor;
	printf("1.Ime            :%s\n", ime == NULL ? "" : ime);
	printf("\n");
	printf("2.Prezime        :%s\n", prezime == NULL ? "" : prezime);
	printf("\n");
	printf("3.Opis posla     :%s\n", opis_posla == NULL ? "" : opis_posla);
	printf("\n");
	printf("4.Korisnicko ime :%s\n", korisnicko_ime == NULL ? "" : korisnicko_ime);
	printf("\n");
	printf("5.Lozinka        :%s\n", hash_lozinka == NULL ? "" : hash_lozinka);
	printf("\n");
	printf("6.Ponovi Lozinku :%s\n", hash_lozinka_2 == NULL ? "" : hash_lozinka_2);
	printf("\n");
	printf("7.Zavrsi formu\n");
	printf("\n");
	printf("8.Izlaz\n");
	printf("#######################################################################################################################\n");
	izbor = _getch();
	switch (izbor) {
	case '1':
		system("cls");
		printf("Ime:");
		ime = unos_2(ime);
		if (ime == NULL) {
			perror("Ime, zauzimanje memorije");
			return 0;
		}
		if (ime != NULL) {
			status_ime = 1;
		}
		return 1;
		break;

	case '2':
		system("cls");
		printf("Prezime:");
		prezime = unos_2(prezime);
		if (prezime == NULL) {
			perror("Prezime, zauzimanje memorije");
			return 0;
		}
		if (prezime != NULL) {
			status_prezime = 1;
		}
		return 1;
		break;
	case '3':
		system("cls");
		printf("Opis posla:");
		opis_posla = unos_2(opis_posla);
		if (opis_posla == NULL) {
			perror("Opis posla, zauzimanje memorije");
			return 0;
		}
		if (opis_posla != NULL) {
			status_opis_posla = 1;
		}
		return 1;
		break;
	case '4':
		do {
			zastavica = 0;
			system("cls");
			printf("Korisnicko ime:");
			korisnicko_ime = unos_2(korisnicko_ime);
			if (korisnicko_ime == NULL) {
				perror("Korisnicko ime, zauzimanje memorije");
				return 0;
			}
			if (provjera_znakova(korisnicko_ime) != 0) {
				printf("Unjeli ste nedozvoljeni znak, za ponovni unos pritisnite [d]");
				if (_getch() == 'd') {
					free(korisnicko_ime);
					korisnicko_ime = NULL;
					zastavica = 1;
				}
				else {
					free(korisnicko_ime);
					korisnicko_ime = NULL;
					zastavica = 0;
				}
			}
			int provjera_korisnickog_imena = 0;
			provjera_korisnickog_imena = provjera_korisnik_istog_imena(korisnicko_ime);
			if (provjera_korisnickog_imena == 0) {
				oslobadjanje_forme(ime, prezime, opis_posla, korisnicko_ime, lozinka, lozinka_2, hash_lozinka, hash_lozinka_2);
				ime = prezime = opis_posla = korisnicko_ime = lozinka = lozinka_2 = hash_lozinka = hash_lozinka_2 = NULL;
				status_ime = status_prezime = status_opis_posla = status_lozinka = status_lozinka_2 = status_korisnicko_ime = 0;
				return 0;
			}
			else if (provjera_korisnickog_imena == 1) {
				printf("Korisnicko ime vec postoji\nza ponovni unos pritisnite [d]");
				if (_getch() == 'd') {
					free(korisnicko_ime);
					korisnicko_ime = NULL;
					zastavica = 1;
				}
				else {
					free(korisnicko_ime);
					korisnicko_ime = NULL;
					zastavica = 0;
				}
			}

		} while (zastavica);
		return 1;
		break;
	case '5':
		lozinka = unos_lozinke_1(lozinka);
		hash_lozinka = sakriji_lozinku(lozinka);
		if (lozinka == NULL) {
			status_lozinka = 0;
			if (lozinka_2 != NULL) {
				free(lozinka_2);
				free(hash_lozinka_2);
				lozinka_2 = hash_lozinka_2 = NULL;
				status_lozinka_2 = 0;
			}
			return 1;
		}
		status_lozinka = 1;
		if (lozinka_2 != NULL) {
			free(lozinka_2);
			free(hash_lozinka_2);
			lozinka_2 = hash_lozinka_2 = NULL;
			status_lozinka_2 = 0;
		}
		return 1;
		break;
	case '6':
		if (status_lozinka == 1) {
			lozinka_2 = unos_lozinke_1(lozinka_2);
			if (usporedba_lozinki(lozinka, lozinka_2) == 1) {
				hash_lozinka_2 = sakriji_lozinku(lozinka_2);
				if (lozinka_2 == NULL) {
					status_lozinka_2 = 0;
					return 1;
				}
				status_lozinka_2 = 1;
				return 1;
			}
			else {
				if (lozinka_2 != NULL) {
					free(lozinka_2);
					free(hash_lozinka_2);
					lozinka_2 = hash_lozinka_2 = NULL;
					status_lozinka_2 = 0;
				}
				return 1;
			}
		}
		else {
			system("cls");
			printf("Prvo unesite lozinku da bi ste je potvrdili\nPritisni bilo koju tipku za nastavak");
			_getch();
			return 1;
		}
		break;
	case '7':
		if (status_ime == 1 && status_prezime == 1 && status_opis_posla == 1 && status_lozinka_2 == 1) {
			printf("Registracija uspjesno obavljena\npritisnite bilo koju tipku");
			int duzina_lozinke = strlen(lozinka);
			lozinka = prosiri_lozinku(lozinka);
			lozinka = hash_lozinke(lozinka, duzina_lozinke);
			dodaj_korisnika(ime, prezime, opis_posla, korisnicko_ime, lozinka);
			oslobadjanje_forme(ime, prezime, opis_posla, korisnicko_ime, lozinka, lozinka_2, hash_lozinka, hash_lozinka_2);
			ime = prezime = opis_posla = korisnicko_ime = lozinka = lozinka_2 = hash_lozinka = hash_lozinka_2 = NULL;
			status_ime = status_prezime = status_opis_posla = status_lozinka = status_lozinka_2 = status_korisnicko_ime = 0;
			return 0;
		}
		else {
			printf("Sva polja moraju biti ispunjena da bi forma bila zavrsena\n");
			printf("Zelite li zavrsiti formu pritisnit[d] za potvrdu u sprotnom svi podaci ce biti obrisani");
			if (_getch() == 'd') {
				return 1;
			}
		}
	case '8':
		if (izlaz() == 1) {
			oslobadjanje_forme(ime, prezime, opis_posla, korisnicko_ime, lozinka, lozinka_2, hash_lozinka, hash_lozinka_2);
			ime = prezime = opis_posla = korisnicko_ime = lozinka = lozinka_2 = hash_lozinka = hash_lozinka_2 = NULL;
			status_ime = status_prezime = status_opis_posla = status_lozinka = status_lozinka_2 = status_korisnicko_ime = 0;
			return 0;
		}
		return 1;
		break;
	case '9':
		printf("\nStatusi:\nStatus ime:%s\t\t%d\nStatus prezime:%s\t\t%d", ime, status_ime, prezime, status_prezime);
		printf("\nStatus opis posla:%s\t%d\nStatus korisnicko ime:%s\t%d\nStatus l1:%s\t\t%d", opis_posla, status_opis_posla, korisnicko_ime, status_korisnicko_ime, lozinka, status_lozinka);
		printf("\nhash lozinka:%s\nStatus l2:%s\t\t%d\nHash lozinka 2:%s", hash_lozinka, lozinka_2, status_lozinka_2, hash_lozinka_2);
		_getch();
		return 1;
		break;
	default:
		return 1;
	}
}