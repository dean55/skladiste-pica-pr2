#define _CRT_SECURE_NO_WARNINGS
#include "deklaracije_funkcija.h"
#include "strukture.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <windows.system.h>

static char* korisnicko_ime = NULL, * lozinka = NULL, * hash_lozinka = NULL;
static int provjera = 0, broj_racuna = 0;
static KORISNIK* racuni = NULL;
int trazeni_id_2=0;

int login_forma(void) {
	char izbor;
	system("cls");
	printf("#######################################################################################################################\n");
	printf("##                                                  PRIJAVA                                                          ##\n");
	printf("#######################################################################################################################\n");
	printf("\n");
	printf("1.Korisnicko ime  :%s\n", korisnicko_ime == NULL ? "" : korisnicko_ime);
	printf("\n");
	printf("2.Lozinka         :%s\n", hash_lozinka == NULL ? "" : hash_lozinka);
	printf("\n");
	printf("3.Prijavi se\n");
	printf("\n");
	printf("4.Prijavi se kao gost\n");
	printf("\n");
	printf("5.Izlaz\n");
	printf("\n");
	printf("#######################################################################################################################\n");
	izbor = _getch();
	switch (izbor){
	case '1':
		system("cls");
		printf("Korisnicko ime:");
		korisnicko_ime = unos_2(korisnicko_ime);
		if (korisnicko_ime == NULL) {
			perror("Korisnicko ime, zauzimanje memorije");
			return 0;
		}
		return 1;
		break;
	case '2':
		lozinka = unos_lozinke_prijava(lozinka);
		hash_lozinka = sakriji_lozinku(lozinka);
		if (lozinka == NULL) {
			return 1;
		}
		return 1;
		break;
	case '3':
		provjera = provjera_admin(korisnicko_ime, lozinka);
		if (provjera == -5) {
			trazeni_id_2 = -1;
			oslobađanje_forme_login(korisnicko_ime, lozinka, hash_lozinka);
			korisnicko_ime = lozinka = hash_lozinka = NULL;
			provjera = broj_racuna = 0;
			return 2;
		}
		provjera = provjera_korisnik_lozinka(korisnicko_ime, lozinka);
		if (provjera == 0) {
			printf("Pogresno korisnicko ime ili lozinka");
			oslobađanje_forme_login(korisnicko_ime, lozinka, hash_lozinka);
			korisnicko_ime = lozinka = hash_lozinka = NULL;
			provjera = broj_racuna = trazeni_id_2 = 0;
			_getch();
			return 1;
		}
		if (provjera == 2) {
			broj_racuna = broj_korisnika();
			racuni = ucitaj_korisnike(racuni, broj_racuna);
			trazeni_id_2=vrati_id(racuni,korisnicko_ime, broj_racuna);
			oslobađanje_forme_login(korisnicko_ime, lozinka, hash_lozinka);
			korisnicko_ime = lozinka = hash_lozinka = NULL;
			provjera = broj_racuna=0 ;
			return 2;
		}
		if (provjera == 3) {
			broj_racuna = broj_korisnika();
			racuni = ucitaj_korisnike(racuni, broj_racuna);
			trazeni_id_2 = vrati_id(racuni, korisnicko_ime, broj_racuna);
			oslobađanje_forme_login(korisnicko_ime, lozinka, hash_lozinka);
			korisnicko_ime = lozinka = hash_lozinka = NULL;
			provjera = broj_racuna=0 ;
			return 3;
		}
		if (provjera == 4) {
			broj_racuna = broj_korisnika();
			racuni = ucitaj_korisnike(racuni, broj_racuna);
			trazeni_id_2 = vrati_id(racuni, korisnicko_ime, broj_racuna);
			oslobađanje_forme_login(korisnicko_ime, lozinka, hash_lozinka);
			korisnicko_ime = lozinka = hash_lozinka = NULL;
			provjera = broj_racuna=0 ;
			return 4;
		}
		return 0;
		break;
	case '4':
		oslobađanje_forme_login(korisnicko_ime, lozinka, hash_lozinka);
		korisnicko_ime = lozinka = hash_lozinka = NULL;
		provjera = 0;
		return 5;
		break;
	case '5':
		if (izlaz() == 1) {
			oslobađanje_forme_login(korisnicko_ime, lozinka, hash_lozinka);
			korisnicko_ime = lozinka = hash_lozinka = NULL;
			provjera = 0;
			return 0;
		}
		return 1;
		break;
	default:
		return 1;
		break;
	}
	
}